# window.py
#
# Copyright 2022 Gerben Droogers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import typing as T
import difflib
import os
import gi

gi.require_version('GtkSource', '5')

from gi.repository import Gtk, Gdk, Gio, GtkSource, Adw

from blueprintcompiler import decompiler, tokenizer, parser
from blueprintcompiler.errors import MultipleErrors, PrintableError
from blueprintcompiler.utils import Colors
from blueprintcompiler.xml_reader import Handler
from xml import sax

from .constants import build_type
from .theme_switcher import ThemeSwitcher



@Gtk.Template(resource_path='/nl/g4d/Blueprintgtk/window.ui')
class BlueprintgtkWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'BlueprintgtkWindow'

    source_view_xml = Gtk.Template.Child()
    source_buffer_xml = Gtk.Template.Child()
    source_buffer_blp = Gtk.Template.Child()
    error_label = Gtk.Template.Child()
    button_menu = Gtk.Template.Child()

    testXML = """<?xml version="1.0" encoding="UTF-8"?>
<interface>
  <requires lib="gtk" version="4.0"/>
  <object class="GtkLabel">
    <property name="label" translatable="true">Hello, world!</property>
  </object>
  <object class="GtkLabel">
    <property name="label" translatable="true" context="translation context">Hello</property>
  </object>
</interface>"""

    def parse(self, string):
        parser = sax.make_parser()
        handler = Handler()
        parser.setContentHandler(handler)
        sax.parseString(string, handler)
        return handler.root


    def __init__(self, **kwargs):
        print(GtkSource.View)
        print(GtkSource.Buffer)
        super().__init__(**kwargs)
        self.__custom_css()
        self.button_menu.get_popover().add_child(ThemeSwitcher(), "themeswitcher");

        # Set devel style
        if build_type == "debug":
            self.get_style_context().add_class("devel")

        language_manager = GtkSource.LanguageManager()
        language_manager.append_search_path("resource:///nl/g4d/Blueprintgtk/language-specs")

        self.source_buffer_xml.set_language(language_manager.get_language("xml"))
        self.source_buffer_xml.set_text(self.testXML)
        self.source_buffer_blp.set_language(language_manager.get_language("blueprint"))

        style_manager = Adw.StyleManager.get_default()
        self.__update_style(style_manager, None)
        style_manager.connect('notify::dark', self.__update_style)

    @Gtk.Template.Callback()
    def on_xml_changed(self, buffer):
        start = buffer.get_start_iter()
        end = buffer.get_end_iter()
        xml = buffer.get_text(start, end, False)
        self.error_label.set_text("")
        self.source_buffer_blp.set_text("")
        try:
            ctx = decompiler.DecompileCtx()
            parser2 = sax.make_parser()
            handler = Handler()
            parser2.setContentHandler(handler)
            sax.parseString(xml, handler)
            parsed = handler.root

            decompiler._decompile_element(ctx, None, parsed)
            decompiled = ctx.result
            try:
                # make sure the output compiles
                tokens = tokenizer.tokenize(decompiled)
                ast, errors, warnings = parser.parse(tokens)
                for warning in warnings:
                    warning.pretty_print("error", decompiled)
                if errors:
                    for error in errors.errors:
                        self.__update_error_label(error.message)
                if len(ast.errors):
                    for error in ast.errors:
                        self.__update_error_label(error.message)
                ast.generate()
            except PrintableError as e:
                e.pretty_print("error", decompiled)
            self.source_buffer_blp.set_text(decompiled)
        except decompiler.UnsupportedError as e:
            self.__update_error_label(e.message)
            if e.tag:
                self.__update_error_label("- in tag " + e.tag)
        except sax.SAXParseException as e:
            self.__update_error_label("%s" % (e))

    def __update_error_label(self, text):
        self.error_label.set_text(self.error_label.get_text() + "\n" + text)

    def __update_style(self, style_manager, dark):
        if style_manager.get_dark():
            style = "Adwaita-dark"
        else:
            style = "Adwaita"
        scheme_manager = GtkSource.StyleSchemeManager()
        self.source_buffer_xml.set_style_scheme(scheme_manager.get_scheme(style))
        self.source_buffer_blp.set_style_scheme(scheme_manager.get_scheme(style))

    def __custom_css(self):
        css_provider = Gtk.CssProvider()
        css_provider_resource = Gio.File.new_for_uri(
            "resource:///nl/g4d/Blueprintgtk/style.css")
        css_provider.load_from_file(css_provider_resource)

        Gtk.StyleContext.add_provider_for_display(Gdk.Display.get_default(), css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        
